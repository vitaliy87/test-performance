# SytleSage Test

This little project is for chech my knowledges ;)

### Download project

```
git clone https://gitlab.com/vitaliy87/test-performance.git
```


### create and activate virtual environment

if you have already installed virtualenv
```
virtualenv -p python3.5 venv
```

if you have not installed virtualenv
```
sudo apt-get install python-virtualenv
virtualenv -p python3.5 venv
```

after that, you have to activate your environment
```
source venv/bin/activate
```

### Install requirements

```
cd test-performance
pip install -r requirements.txt
```

### Run project

```
./manage.py runserver
```

## Test API's

without authentication
```
http://127.0.0.1:8000/music/artists/
```
with authentication
```
http://127.0.0.1:8000/music/artists/1/
http://127.0.0.1:8000/music/albums/
http://127.0.0.1:8000/music/albums/details/
```

## Get Credentials

to get authentication you have to login in next endpoint
```
http://127.0.0.1:8000/api-token-auth/
```
or get it with curl but, changing credentials provided by mail:
```
curl -d '{"username":"username", "password":"password"}' -H "Content-Type: application/json" -X POST http://127.0.0.1:8000/api-token-auth/
```
copy your token and past it in postman

![Alt Text](https://miro.medium.com/max/1200/1*2gnUYOHgGr_lcCbchf8rBQ.gif)

You have attached a postman collection with all API's *test performance.postman_collection.json* the unique thing you have to do is import it in postman and use it, of cource previously you have to generate your own JWT token and use it in this postman-collection

## Test Performance of API's with Silk

I am using SILK to test sql querys and performance of API

```
http://127.0.0.1:8000/silk/requests/
```

