from django.db.models import Count, Sum, Prefetch, F
from django.http import Http404
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework import status
from .models import Artists, Albums, Tracks
from .serializers import (
    ArtistsSerializer,
    AlbumsSerializer,
    ArtistAlbumsSerializer,
    AlbumsDetailsSerializer
)

import json


class ArtistsView(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        artists = Artists.objects
        serialized_artists = ArtistsSerializer(artists, many=True)

        return Response(serialized_artists.data, status=status.HTTP_200_OK)


class ArtistAlbumsView(APIView):

    def get_queryset(self, pk):
        try:
            queryset = Artists.objects.prefetch_related(
                "Albums").get(ArtistId=pk)
        except Artists.DoesNotExist:
            raise Http404

        return queryset

    def get(self, request, pk):
        queryset = self.get_queryset(pk)
        serialized_artist_albums = ArtistAlbumsSerializer(queryset)

        return Response(serialized_artist_albums.data, status=status.HTTP_200_OK)


class AlbumsView(APIView):

    def get_queryset(self):

        return Albums.objects.prefetch_related("Tracks").all()

    def get(self, request):
        queryset = self.get_queryset()
        serialized_albums = AlbumsSerializer(queryset, many=True)

        return Response(serialized_albums.data, status=status.HTTP_200_OK)


class AlbumsDetailsView(APIView):

    def get_queryset(self):

        return Albums.objects.annotate(Tracks_Count=Count(
            "Tracks"), Album_Duration=Sum(
            "Tracks__Milliseconds"), Artist_Name=F(
            'ArtistId__Name')).prefetch_related(Prefetch(
                "Tracks", queryset=Tracks.objects.order_by(
                    "Milliseconds"))).all()

    def get(self, request):
        queryset = self.get_queryset()
        serialized_albums = AlbumsDetailsSerializer(queryset, many=True)

        return Response(serialized_albums.data, status=status.HTTP_200_OK)
