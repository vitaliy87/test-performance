from rest_framework import serializers
from .models import (
    Artists, Albums, Tracks
)


class ArtistsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Artists
        fields = '__all__'


class TracksSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tracks
        fields = '__all__'


class AlbumsSerializer(serializers.ModelSerializer):
    Tracks = TracksSerializer(many=True, read_only=True)

    class Meta:
        model = Albums
        fields = ('AlbumId', 'Title', 'ArtistId', 'Tracks')


class AlbumsOnlySerializer(serializers.ModelSerializer):

    class Meta:
        model = Albums
        fields = ('AlbumId', 'Title')


class ArtistAlbumsSerializer(serializers.ModelSerializer):
    Albums = AlbumsOnlySerializer(many=True, read_only=True)

    class Meta:
        model = Artists
        fields = ('ArtistId', 'Name', 'Albums')


class AlbumsDetailsSerializer(serializers.ModelSerializer):
    Tracks = TracksSerializer(many=True, read_only=True)
    Artist_Name = serializers.SerializerMethodField(required=False)
    Tracks_Count = serializers.SerializerMethodField(required=False)
    Album_Duration = serializers.SerializerMethodField(required=False)
    Shortest_Track = serializers.SerializerMethodField(required=False)
    Longest_Track = serializers.SerializerMethodField(required=False)

    def get_Artist_Name(self, obj):
        return obj.Artist_Name

    def get_Tracks_Count(self, obj):
        return obj.Tracks_Count

    def get_Album_Duration(self, obj):
        return obj.Album_Duration

    def get_Shortest_Track(self, obj):
        track = list(obj.Tracks.all())[0]
        return TracksSerializer(track, read_only=True).data

    def get_Longest_Track(self, obj):
        track = list(obj.Tracks.all())[-1]
        return TracksSerializer(track, read_only=True).data

    class Meta:
        model = Albums
        fields = (
            'AlbumId',
            'Title',
            'ArtistId',
            'Tracks',
            'Artist_Name',
            'Tracks_Count',
            'Album_Duration',
            'Shortest_Track',
            'Longest_Track'
        )
