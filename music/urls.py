from django.urls import path
from .views import (
    ArtistsView,
    ArtistAlbumsView,
    AlbumsView,
    AlbumsDetailsView
)
from django.conf.urls import url
urlpatterns = [
    url(r'artists/(?P<pk>[0-9]+)/$', ArtistAlbumsView.as_view()),
    url(r'artists/', ArtistsView.as_view()),
    url(r'albums/details/', AlbumsDetailsView.as_view()),
    url(r'albums/', AlbumsView.as_view()),

]
