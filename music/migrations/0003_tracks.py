# Generated by Django 2.2.6 on 2019-10-19 21:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('music', '0002_albums'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tracks',
            fields=[
                ('TrackId', models.AutoField(primary_key=True, serialize=False)),
                ('Name', models.CharField(max_length=255, null=True)),
                ('AlbumId', models.IntegerField(null=True)),
                ('MediaTypeId', models.IntegerField(null=True)),
                ('GenreId', models.IntegerField(null=True)),
                ('Composer', models.CharField(max_length=255, null=True)),
                ('Milliseconds', models.IntegerField(blank=True, null=True)),
                ('Bytes', models.IntegerField()),
                ('UnitPrice', models.FloatField(blank=True, default=0, null=True)),
            ],
        ),
    ]
