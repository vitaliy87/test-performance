from django.db import models


class Artists(models.Model):
    ArtistId = models.AutoField(primary_key=True)
    Name = models.CharField(max_length=255, null=False, default='')

    def __str__(self):
        return '{}, {}'.format(self.ArtistId, self.Name)


class Albums(models.Model):
    AlbumId = models.AutoField(primary_key=True)
    Title = models.CharField(max_length=255, null=False, default='')
    ArtistId = models.ForeignKey(
        Artists, related_name='Albums', on_delete=models.DO_NOTHING)

    def __str__(self):
        return '{}, {}'.format(self.AlbumId, self.Title)


class MediaTypes(models.Model):
    MediaTypeId = models.AutoField(primary_key=True)
    Name = models.CharField(max_length=255, null=False, default='')

    def __str__(self):
        return '{}, {}'.format(self.MediaTypeId, self.Name)


class Genres(models.Model):
    GenreId = models.AutoField(primary_key=True)
    Name = models.CharField(max_length=255, null=False, default='')

    def __str__(self):
        return '{}, {}'.format(self.GenreId, self.Name)

    # AlbumId = models.ForeignKey(Albums, on_delete=models.DO_NOTHING)
    # MediaTypeId = models.ForeignKey(MediaTypes, on_delete=models.DO_NOTHING)
    # GenreId = models.ForeignKey(Genres, on_delete=models.DO_NOTHING)


class Tracks(models.Model):
    TrackId = models.AutoField(primary_key=True)
    Name = models.CharField(max_length=255, null=True)
    AlbumId = models.ForeignKey(
        Albums, related_name='Tracks', on_delete=models.DO_NOTHING, null=True)
    MediaTypeId = models.ForeignKey(
        MediaTypes, related_name='Tracks', on_delete=models.DO_NOTHING, null=True)
    GenreId = models.ForeignKey(
        Genres, related_name='Tracks', on_delete=models.DO_NOTHING, null=True)
    Composer = models.CharField(max_length=255, null=True)
    Milliseconds = models.IntegerField(blank=True, null=True)
    Bytes = models.IntegerField()
    UnitPrice = models.FloatField(default=0, blank=True, null=True)

    def __str__(self):
        return '{}, {}'.format(self.TrackId, self.Name)
